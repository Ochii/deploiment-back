import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MessageEntity } from './entity/message.entity';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.HOST_NAME,
      port: parseInt(process.env.PORT_BACK),
      username: process.env.USER_NAME,
      password: process.env.USER_PASSWORD,
      database: process.env.DB_NAME,
      entities: [MessageEntity],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([MessageEntity]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
